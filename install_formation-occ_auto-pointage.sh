#!/usr/bin/bash

# Auteur : Silejonu
# E-mail : silejonu@tutanota.com
# Source : https://codeberg.org/Silejonu/formation-occ_auto-pointage
# Dernière modification : 2022-11-24

if [[ $UID == 0 ]] ; then
  echo 'Erreur : Ce script ne doit pas être lancé avec les privilèges du super-utilisateur.'
  exit 1
fi

for dependance in awk curl grep ; do
  if ! which ${dependance} &> /dev/null ; then
    echo "Dépendance manquante : ${dependance}. Veuillez l’installer."
    exit 2
  fi
done

echo
echo 'Veuillez renseigner vos identifiants de la plateforme Occitanie E-Formation.'
read -rp 'Nom d’utilisateur : ' nom_d_utilisateur 
read -rp 'Mot de passe : ' mot_de_passe
echo

mkdir -p ~/.local/share/formation-occ_auto-pointage
echo "${nom_d_utilisateur}" > ~/.local/share/formation-occ_auto-pointage/identifiants
echo "${mot_de_passe}" >> ~/.local/share/formation-occ_auto-pointage/identifiants

# Installation du script principal
curl --no-progress-meter https://codeberg.org/Silejonu/formation-occ_auto-pointage/raw/branch/main/formation-occ_auto-pointage.sh --output formation-occ_auto-pointage.sh

verifier_notify-send() {
if ! which notify-send &> /dev/null ; then
  echo
  echo 'notify-send est nécessaire pour le support des notifications et n’est pas installé.'
  echo 'Si vous ne savez pas comment l’installer, suivez ces instructions :'
  echo 'https://codeberg.org/Silejonu/formation-occ_auto-pointage/src/branch/main/README.md#1-3-installer-le-support-des-notifications'
  echo
fi
}

read -rp 'Voulez-vous activer les notifications de réussite et d’échec du pointage (recommandé) ? [O/n] ' oui_non
case ${oui_non} in
  [nN]|[nN][oO]|[nN][oO][nN]) sed -i 's/^#exit/exit/' formation-occ_auto-pointage.sh ;;
  *) verifier_notify-send ;;
esac

sudo install --mode 755 formation-occ_auto-pointage.sh /usr/local/bin/

# Installation du service et du minuteur systemd
mkdir -p ~/.config/systemd/user

curl --no-progress-meter https://codeberg.org/Silejonu/formation-occ_auto-pointage/raw/branch/main/formation-occ_auto-pointage.service --output formation-occ_auto-pointage.service
install --mode 644 formation-occ_auto-pointage.service ~/.config/systemd/user/

curl --no-progress-meter https://codeberg.org/Silejonu/formation-occ_auto-pointage/raw/branch/main/formation-occ_auto-pointage.timer --output formation-occ_auto-pointage.timer
install --mode 644 formation-occ_auto-pointage.timer ~/.config/systemd/user/

systemctl --user daemon-reload &> /dev/null
systemctl --user enable --now formation-occ_auto-pointage.timer &> /dev/null

echo
echo 'Installation terminée.'
