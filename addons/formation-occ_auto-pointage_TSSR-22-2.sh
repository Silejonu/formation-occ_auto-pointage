#!/usr/bin/bash

agenda="${HOME}/.local/share/formation-occ_auto-pointage/planning.csv"
cours_du_jour=$(grep -m1 "$(date +%d/%m/%Y)" "${agenda}" | cut -d';' -f6)

# Si le fichier d’agenda existe…
if [[ -f "${agenda}" ]] ; then
  # … et qu’aujourd’hui n’est pas un jour de tutorat…
  if ! [[ ${cours_du_jour} =~ TUT ]] ; then
    # … alors arrêter le script.
    notify-send --icon=x-office-calendar-symbolic 'Occitanie E-Formation' 'Aujourd’hui n’est pas un tutorat, pas de pointage automatique.'
    exit
  fi
fi