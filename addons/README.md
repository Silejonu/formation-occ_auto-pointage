# Addons

## TSSR-22-2

*Cet addon limite le pointage aux jours de tutorat.*

### Installation

```bash
( cd $(mktemp -d)
wget https://codeberg.org/Silejonu/formation-occ_auto-pointage/raw/branch/main/addons/formation-occ_auto-pointage_TSSR-22-2.sh
sudo install --mode 755 formation-occ_auto-pointage_TSSR-22-2.sh /usr/local/bin/ )
```

Placez ensuite le fichier [`planning.csv`](https://www.campus-numerique.eu/ldnr/pf6/mod/resource/view.php?id=3280) dans le répertoire `~/.local/share/formation-occ_auto-pointage/`.  
Si ce fichier est absent du répertoire, alors cet addon n’a aucun effet.

### Activer/désactiver les notifications

```bash
sudo sed -i 's/ #notify-send/ notify-send/' /usr/local/bin/formation-occ_auto-pointage_TSSR-22-2.sh # activer les notifications
```
```bash
sudo sed -i 's/ notify-send/ #notify-send/' /usr/local/bin/formation-occ_auto-pointage_TSSR-22-2.sh # désactiver les notifications
```

### Désinstallation

```bash
rm ~/.local/share/formation-occ_auto-pointage/planning.csv
sudo rm /usr/local/bin/formation-occ_auto-pointage_TSSR-22-2.sh
```