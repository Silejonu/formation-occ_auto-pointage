# Occitanie E-Formation auto-pointage

Pointage automatique sur la plateforme [Occitanie E-Formation](https://www.formation-occ.com/).

Si les notifications sont activées, le script confirmera la réussite ou l’échec du pointage.

# Sommaire

- 1. [Installation](#1-installation)
    - 1.1 [Prérequis](#1-1-pr%C3%A9requis)
    - 1.2 [Instructions d’installation](#1-2-instructions-d-installation)
    - 1.3 [Installer le support des notifications](#1-3-installer-le-support-des-notifications)
- 2. [Usage](#2-usage)
    - 2.1 [Modifier les horaires de pointage](#2-1-modifier-les-horaires-de-pointage)
    - 2.2 [Modifier l’identifiant et le mot de passe](#2-2-modifier-l-identifiant-et-le-mot-de-passe)
    - 2.3 [Activer/désactiver les notifications](#2-3-activer-d%C3%A9sactiver-les-notifications)
- 3. [Addons](#3-addons)
- 4. [Désinstallation](#4-d%C3%A9sinstallation)

## 1. Installation

### 1.1 Prérequis

- Distribution Linux avec systemd *(si vous ne savez pas ce qu’est systemd, il y a toutes les chances pour que votre distribution l’utilise)*
- `curl`
- `awk`
- `grep`
- `notify-send` *(optionnel, pour le support des notifications)*

### 1.2 Instructions d’installation

```bash
( cd $(mktemp -d)
curl https://codeberg.org/Silejonu/formation-occ_auto-pointage/raw/branch/main/install_formation-occ_auto-pointage.sh --output install_formation-occ_auto-pointage.sh --no-progress-meter
bash ./install_formation-occ_auto-pointage.sh )
```

### 1.3 Installer le support des notifications

*Cette étape n’est nécessaire que si vous avez activé les notifications et que le script d’installation vous signale que `notify-send` n’est pas installé.*

```bash
sudo apt install libnotify-bin # pour Debian / Ubuntu / Linux Mint et dérivées
```
```bash
sudo dnf install libnotify # pour Fedora / CentOS / Red Hat Enterprise Linux et dérivées
```
```bash
sudo pacman -S libnotify # pour Arch Linux / Manjaro et dérivées
```
```bash
sudo zypper install libnotify # pour openSUSE / SUSE Linux Enterprise et dérivées
```

## 2. Usage

Le script de pointage se lancera automatiquement du lundi au vendredi à 8h31 et 13h31, avec un délai aléatoire de 20 minutes maximum (pour éviter de saturer le site de requêtes simultanées). Si votre ordinateur est éteint lors du moment de l’activation du script, il se lancera à l’allumage de l’ordinateur. Si les notifications sont activées, le script confirmera la réussite ou l’échec du pointage.

### 2.1 Modifier les horaires de pointage

Éditez le fichier `~/.config/systemd/user/formation-occ_auto-pointage.timer`, et modifiez les lignes suivantes par [les valeurs de votre choix](https://wiki.archlinux.org/title/Systemd/Timers#Realtime_timer) :
```
OnCalendar=Mon..Fri 08:31
OnCalendar=Mon..Fri 13:31
```

Le délai aléatoire maximum (en secondes) peut être ajusté en éditant la ligne suivante :
```
AccuracySec=1200
```

### 2.2 Modifier l’identifiant et le mot de passe

Éditez le fichier `~/.local/share/formation-occ_auto-pointage/identifiants`, en respectant le format suivant :
```
dupond.ca
mon_mot_de_passe
```

### 2.3 Activer/désactiver les notifications

```bash
sudo sed -i 's/^exit/#exit/' /usr/local/bin/formation-occ_auto-pointage.sh # activer les notifications
```
```bash
sudo sed -i 's/^#exit/exit/' /usr/local/bin/formation-occ_auto-pointage.sh # désactiver les notifications
```

## 3. Addons

Il est possible d’ajouter des addons au script, qui seront exécutés avant celui-ci (par exemple pour lancer le script uniquement les jours correspondant à un certain type de cours).  
Pour cela, ajoutez un fichier `formation-occ_auto-pointage_{nom de l’addon}.sh` dans le répertoire `/usr/local/bin/`, et donnez-lui les droits `755`.

[Consulter les addons existants.](https://codeberg.org/Silejonu/formation-occ_auto-pointage/src/branch/main/addons)  
*N’hésitez pas à soumettre vos addons si vous en avez créé pour les rajouter à la liste.*

## 4. Désinstallation

```bash
sudo rm /usr/local/bin/formation-occ_auto-pointage*
rm -rf ~/.local/share/formation-occ_*
systemctl --user disable --now formation-occ_auto-pointage.{service,timer}
rm ~/.config/systemd/user/formation-occ_auto-pointage.{service,timer}
systemctl --user daemon-reload
sudo systemctl daemon-reload
```
