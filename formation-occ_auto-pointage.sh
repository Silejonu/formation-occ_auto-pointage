#!/usr/bin/bash

# Auteur : Silejonu
# E-mail : silejonu@tutanota.com
# Source : https://codeberg.org/Silejonu/formation-occ_auto-pointage
# Dernière modification : 2022-11-24

# Exécute les addons
for addon in $(ls /usr/local/bin/formation-occ_auto-pointage_*.sh) ; do
  source "${addon}"
done

fichier_identifiants=${HOME}/.local/share/formation-occ_auto-pointage/identifiants
fichier_cookies=${HOME}/.local/share/formation-occ_auto-pointage/cookies
fichier_verification_pointage=$(mktemp)

# Vérifier que le site « formation-occ.com » est bien fonctionnel, et attendre si ce n’est pas le cas
until [[ $(curl --silent --head "https://www.formation-occ.com/" | awk '/HTTP\// {print $2}') == 200 ]] ; do
  sleep 30
done

# Supprimer les lignes vides du fichier formation-occ_identifiants s’il y en a
sed -i '/^$/d' ${fichier_identifiants}

# Envoyer le nom d’utilisateur et le mot de passe vers la page de connexion, puis récupérer les cookies
curl --cookie-jar ${fichier_cookies} --data-urlencode "username=$(head --lines=1 ${fichier_identifiants})" --data-urlencode "password=$(tail --lines=1 ${fichier_identifiants})" https://www.formation-occ.com/connexion.php

# Accéder à la page de pointage en utilisant les cookies de connexion
curl --location --cookie ${fichier_cookies} https://www.formation-occ.com/insert_attendance_event.php > ${fichier_verification_pointage} 

# Décommenter/commenter la ligne suivante pour recevoir ou non les notifications de pointage
#exit

# Vérifier que le pointage a bien été effectué
if [[ $(grep -i 'Votre pointage a été enregistré' ${fichier_verification_pointage}) ]] ; then
  notify-send --icon=emblem-ok-symbolic 'Occitanie E-Formation' 'Pointage effectué avec succès.'
else
  notify-send --icon=emblem-important-symbolic 'Occitanie E-Formation' 'Erreur : le pointage n’a pas pu être effectué automatiquement.'
fi
